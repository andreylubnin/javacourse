package lesson7_homework;

public class Cat {

    private String name;
    private int appetite;
    private boolean satiety;

    public Cat(String name, int appetite) {
        this.name = name;
        this.appetite = appetite;
        this.satiety = false;
    }

    public String getName () {
        return name;
    }

    public int getAppetite () {
        return appetite;
    }

    public void eat(Plate plate) {
        if(plate.decreaseFood(appetite)){
            this.satiety = true;
        }

    }


}
