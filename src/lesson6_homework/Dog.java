package lesson6_homework;

public class Dog extends Animal implements IJump {
    public Dog(String name, int age) {
        super(name, age);
    }

    private final int LIMIT_RUN = 500;
    private final int LIMIT_SWIM = 10;

    @Override
    public void jump() {

    }

    @Override
    public void run(int obstacleLength, String name) {
        if(obstacleLength <= LIMIT_RUN) {
            super.run(obstacleLength, name);
        } else {
            super.run(LIMIT_RUN, name);
        }
    }

    @Override
    public void swim(int obstacleLength, String name) {
        if(obstacleLength <= LIMIT_SWIM) {
            super.swim(obstacleLength, name);
        } else {
            super.swim(LIMIT_SWIM, name);
        }

    }
}
