package lesson6_homework;

public class Cat extends Animal implements IJump{
    public Cat(String name, int age) {
        super(name, age);
    }

    private final int LIMIT_RUN = 500;

    @Override
    public void jump() {

    }

    @Override
    public void run(int obstacleLength, String name) {
        if(obstacleLength <= LIMIT_RUN) {
            super.run(obstacleLength, name);
        } else {
            super.run(LIMIT_RUN, name);
        }
    }

    @Override
    public void swim(int obstacleLength, String name){
        System.out.println(name + " отказался плыть совсем - кошки не умеют плавать!");
    }

}
