package lesson6_homework;


public class Main {
    public static int actionLength;

    public static void main(String[] args) {

        Dog dog1 = new Dog("Ася", 10);
        Dog dog2 = new Dog("Тери", 9);
        Dog dog3 = new Dog("Клык", 7);
        Dog dog4 = new Dog("Чиля", 5);

        Cat cat1 = new Cat("Барсик", 1);
        Cat cat2 = new Cat("Мурзик", 5);
        Cat cat3 = new Cat("Руслан", 7);

        Animal animal1 = new Cat("Матроскин", 16);

        Animal[] animals = new Animal[]{dog1,dog2,dog3,dog4,cat1,cat2,cat3,animal1};

        doActions(animals);

    }

    private static void doActions(Animal... animals) {
        int catsCount, dogsCount, animalCount, objectCount;
        catsCount = dogsCount = animalCount = objectCount = 0;
        for (Animal animal : animals) {
            actionLength = (int)(Math.random()*1000);
            animal.run(actionLength, animal.getName());
            animal.swim(actionLength, animal.getName());
            if(animal instanceof Cat)
                catsCount++;
            else if (animal instanceof Dog)
                 dogsCount++;
            else
                animalCount++;
        }
        System.out.println("Всего котов: "+catsCount);
        System.out.println("Всего собак: "+dogsCount);
        System.out.println("Всего других животных: "+animalCount);
        System.out.println("Всего объектов: "+objectCount);
    }
}
