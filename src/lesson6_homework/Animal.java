package lesson6_homework;

public abstract class Animal {
    private String name;
    private int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void run(int obstacleLength, String name){
        System.out.println(name + " пробежал "+ obstacleLength + " м.");
    }

    public void swim(int obstacleLength, String name){
        System.out.println(name + " проплыл "+ obstacleLength + " м.");
    }

    public void jump(int obstacleLength, String name){
        System.out.println(name + " прыгнул на "+ obstacleLength + " м.");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
