package lesson4;

import java.sql.SQLOutput;
import java.util.Random;
import java.util.Scanner;

public class TicTacToe {

    public static final int SIZE = 3;
    public static final int DOTS_TO_WIN = 3;

    public static final char DOT_EMPTY = '·';
    public static final char DOT_X = 'X';
    public static final char DOT_O = 'O';
    public static final String EMPTY_COLUMN = "  ";
    public static final String EMPTY = " ";

    public static char[][] map = new char[SIZE][SIZE];;

    public static Random random = new Random();
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        initMap();
        printField();

        playGame();
    }

    private static void playGame() {

        while(true) {
            // ход человека
            humanTurn();
            printField();
            if(checkEnd(DOT_X, "Вы победили!"))
                System.exit(0);

            // ход ИИ
            aiTurn();
            printField();
            if(checkEnd(DOT_O, "Компьютер победил!"))
                System.exit(0);
        }
    }

    private static boolean checkEnd(char symbol, String winMessage){

        // проверка на победу
        if(checkWin(symbol)) {
            System.out.println(winMessage);
            return true;
        }

        // проверка на заполненность карты
        if(isMapFull()) {
            System.out.println("Ничья!");
            return true;
        }
        return false;
    }

    private static boolean isMapFull() {
        for (char[] chars : map) {
            for (char aChar : chars) {
                if(aChar==DOT_EMPTY)
                    return false;
            }
        }
        return true;
    }

    private static boolean checkWin(char symbol) {
        if(map[0][0]==symbol && map[0][1]==symbol && map[0][2]==symbol) return true;
        if(map[1][0]==symbol && map[1][1]==symbol && map[1][2]==symbol) return true;
        if(map[2][0]==symbol && map[2][1]==symbol && map[2][2]==symbol) return true;
        if(map[0][0]==symbol && map[1][0]==symbol && map[2][0]==symbol) return true;
        if(map[0][1]==symbol && map[1][1]==symbol && map[2][1]==symbol) return true;
        if(map[0][2]==symbol && map[1][2]==symbol && map[2][2]==symbol) return true;
        if(map[0][0]==symbol && map[1][1]==symbol && map[2][2]==symbol) return true;
        if(map[2][0]==symbol && map[1][1]==symbol && map[0][2]==symbol) return true;
        return false;
    }

    private static void humanTurn() {
        int rowNumber, colNumber;

        do {
            System.out.println("\n Ход пользователя. Введите номера строки и столбца.");
            System.out.print("Строка = ");
            rowNumber = scanner.nextInt();
            System.out.print("Столбец = ");
            colNumber = scanner.nextInt();
        } while (!isCellValid(rowNumber, colNumber));

        map[rowNumber-1][colNumber-1] = DOT_X;

    }

    private static void aiTurn() {
        int rowNumber, colNumber;
        System.out.println("\n Ход машины.");
        do {
            rowNumber = random.nextInt(SIZE)+1;
            colNumber = random.nextInt(SIZE)+1;
        } while (!isCellValid(rowNumber, colNumber));

        map[rowNumber-1][colNumber-1] = DOT_O;

    }

    private static boolean isCellValid(int rowNumber, int colNumber) {
      if((rowNumber< 1 || rowNumber > SIZE) ||
        (colNumber< 1 || colNumber > SIZE)
        ) {
          System.out.println("\n Проверьте значения строки/столбца!");
          return false;
      }
      if(map[rowNumber-1][colNumber-1]!=DOT_EMPTY) {
          System.out.println("\n Указанная ячейка уже занята!");
          return false;
      }
      return true;
    };

    private static void initMap() {

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                map[i][j] = DOT_EMPTY;
            }
        }
    }

    private static void printField() {
        System.out.print(EMPTY_COLUMN);
        for (int i = 0; i < SIZE; i++) {
            System.out.print(i+1+EMPTY);
        }

        for (int i = 0; i < SIZE; i++) {
            System.out.print("\n"+(i+1)+EMPTY);
            for (int j = 0; j < SIZE; j++) {
                System.out.print(map[i][j]+EMPTY);
            }
        }
        System.out.print("\n");
    }


}
