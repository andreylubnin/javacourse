package lesson4;

import java.util.Random;
import java.util.Scanner;

public class TicTacToe_homework {

    public static int SIZE = 3;
    public static int DOTS_TO_WIN = 3;

    public static final char DOT_EMPTY = '·';
    public static final char DOT_X = 'X';
    public static final char DOT_O = 'O';
    public static final String EMPTY_COLUMN = "  ";
    public static final String EMPTY = " ";

    public static char[][] map;

    public static Random random = new Random();
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        askHuman();
        initMap();
        printField();
        playGame();

    }

    private static void askHuman() {
        System.out.print("\nДавайте определим размер поля. На каком поле желаете сразиться? Укажите размер стороны поля: ");
        SIZE = scanner.nextInt();
        System.out.print("\nПрекрасно! А теперь давайте определим, сколько ячеек подряд являются выигрышной комбинацией?. Укажите длину: ");
        DOTS_TO_WIN = scanner.nextInt();
        System.out.println("\nОтлично! Будем сражаться на поле " + SIZE + "x" + SIZE + ". Выиграет тот, кто первым заполнит "+ DOTS_TO_WIN + " ячеек подряд.");
        map = new char[SIZE][SIZE];
    }

    private static void playGame() {

        while(true) {
            // ход человека
            humanTurn();
            printField();
            if(checkEnd(DOT_X, DOT_O, "Вы победили!"))
                System.exit(0);

            // ход ИИ
            aiTurn();
            printField();
            if(checkEnd(DOT_O, DOT_X, "Компьютер победил!"))
                System.exit(0);
        }
    }

    private static boolean checkEnd(char symbol, char enemySymbol, String winMessage){

        // проверка на победу
        if(checkWin(symbol, enemySymbol)) {
            System.out.println(winMessage);
            return true;
        }

        // проверка на заполненность карты
        if(isMapFull()) {
            System.out.println("Ничья!");
            return true;
        }
        return false;
    }

    private static boolean isMapFull() {
        for (char[] chars : map) {
            for (char aChar : chars) {
                if(aChar==DOT_EMPTY)
                    return false;
            }
        }
        return true;
    }

    private static boolean checkWin(char symbol, char enemySymbol) {
        String[][] runnerh = new String[SIZE][SIZE];
        String[][] runnerv = new String[SIZE][SIZE];
        String[][] runnerd = new String[SIZE][SIZE];

        for (int i = 0; i < SIZE; i++) {  // строка
            for (int j = 0; j < SIZE-DOTS_TO_WIN+1; j++) { // столбец
                runnerh[i][j]="";
                runnerv[j][i]="";
                for (int k = 0; k < DOTS_TO_WIN; k++) {
                    runnerh[i][j] += map[i][j+k];
                    runnerv[j][i] += map[i][j+k];
                }
                if(!runnerh[i][j].contains(String.valueOf(enemySymbol)) && !runnerh[i][j].contains(String.valueOf(DOT_EMPTY))) return true;
                if(!runnerv[j][i].contains(String.valueOf(enemySymbol)) && !runnerv[j][i].contains(String.valueOf(DOT_EMPTY))) return true;
            }
        }

        for (int i = 0; i < SIZE-DOTS_TO_WIN+1; i++) {  // столбец и строка
            runnerd[i][i]="";
            for (int k = 0; k < DOTS_TO_WIN; k++) {
                runnerd[i][i] += map[i+k][i+k];
            }
            if(!runnerd[i][i].contains(String.valueOf(enemySymbol)) && !runnerd[i][i].contains(String.valueOf(DOT_EMPTY))) return true;
        }
        for (int i = SIZE-1; i >= DOTS_TO_WIN-1 ; i--) {  // столбец и строка
            runnerd[i][i]="";
            for (int k = 0; k < DOTS_TO_WIN; k++) {
                runnerd[i][i] += map[i-k][i-k];
            }
            if(!runnerd[i][i].contains(String.valueOf(enemySymbol)) && !runnerd[i][i].contains(String.valueOf(DOT_EMPTY))) return true;
        }
        return false;
    }

    private static void humanTurn() {
        int rowNumber, colNumber;

        do {
            System.out.println("\n Ход пользователя. Введите номера строки и столбца.");
            System.out.print("Строка = ");
            rowNumber = scanner.nextInt();
            System.out.print("Столбец = ");
            colNumber = scanner.nextInt();
        } while (!isCellValid(rowNumber, colNumber));

        map[rowNumber-1][colNumber-1] = DOT_X;

    }

    private static void aiTurn() {
        int rowNumber, colNumber;
        System.out.println("\n Ход машины.");
        do {
            rowNumber = random.nextInt(SIZE)+1;
            colNumber = random.nextInt(SIZE)+1;
        } while (!isCellValid(rowNumber, colNumber));

        map[rowNumber-1][colNumber-1] = DOT_O;

    }

    private static boolean isCellValid(int rowNumber, int colNumber) {
      if((rowNumber< 1 || rowNumber > SIZE) ||
        (colNumber< 1 || colNumber > SIZE)
        ) {
          System.out.println("\n Проверьте значения строки/столбца!");
          return false;
      }
      if(map[rowNumber-1][colNumber-1]!=DOT_EMPTY) {
          System.out.println("\n Указанная ячейка уже занята!");
          return false;
      }
      return true;
    };

    private static void initMap() {

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                map[i][j] = DOT_EMPTY;
            }
        }
    }

    private static void printField() {
        System.out.print(EMPTY_COLUMN);
        for (int i = 0; i < SIZE; i++) {
            System.out.print(i+1+EMPTY);
        }

        for (int i = 0; i < SIZE; i++) {
            System.out.print("\n" + ((SIZE > 9 ) ? EMPTY : "") + (i+1)+EMPTY);
            for (int j = 0; j < SIZE; j++) {
                System.out.print(map[i][j]+EMPTY);
            }
        }
        System.out.print("\n");
    }


}
