package lesson3;

public class Main {
    public static void main(String[] args) {
        String str1 = "Java";
        String str2 = "Java";
        String str3 = new String ("Java");

        System.out.println("str1 == str2 :" + (str1 == str2));
        System.out.println("str1 == str3 :" + (str1 == str3));
        System.out.println("str1 == str3 :" + (str1.equals(str3)));

        double a = 1.0;
        double b = 45.3;

        System.out.println(getX(a, b));

        int a1 = 4345345;
        int b2 = 453475345;
        System.out.println(getX(a1, b2));

        getX();

        System.out.println(getX(3,2,1,6));
    }

    static double getX(int a, int... other) {
        int sum = 0;
        for (int i : other) {
            sum += i;
        }
        return 1.0 * sum / a * 100;
    }

    static double getX(double a, double b) {
        return 1.0 * a / b * 100;
    }

    static void getX() {
        System.out.println("Вы не ввели аргументы");
    }
}
