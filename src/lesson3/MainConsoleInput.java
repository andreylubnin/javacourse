package lesson3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class MainConsoleInput {
    public static void main(String[] args) throws IOException {

/*        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter text: ");
        String line = reader.readLine();
        System.out.println("Line from console: "+line);*/

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter text: ");
        String line = scanner.nextLine();
        System.out.println("Line from console: "+line);


        System.out.print("Enter number: ");
        int str = scanner.hasNextInt() ? scanner.nextInt() : 0;
        System.out.printf("\n Line from console: %d! We are happy", str);
    }
}
