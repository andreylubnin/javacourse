package lesson3;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Date2 {
    public static void main(String[] args) throws InterruptedException {
        Date date = new Date();
        long time = date.getTime();

        System.out.println(time);
        for (; time < date.getTime()+2;time++) {
            System.out.println("Еще не пришло время");
            Thread.sleep(1000);
        }
        System.out.println("ВРЕМЯ!!! ");
        date.setTime(time);
        SimpleDateFormat format = new SimpleDateFormat("dd MM yyyy, hh:mm:ss");

        System.out.println("Текущее время: " + format.format(time));
    }
}
