package lesson6;

import lesson1.Calculator;
import lesson5.User;
import lesson5_homework.Employee;

public class TestClass {
    public static void main(String[] args) {
 //       Cat cat = new Cat("Мартин", "черный",  1, 6);
        Dog dog = new Dog("Гендальф", "серый", 4, 1);

        Animal cat = new Cat("Мартин", "черный",
                1, 6);

        Object[] animals = new Object[] {cat, dog, new Employee("Lapochka",
                "Chameleon", null,null,1)};

        dog.changeColor("белый");

        printVoices(cat, dog);
        printVoices(animals);
//        dog.printInfo();
//        cat.printInfo();
    }

    private static void printVoices(Animal... animals) {
        for (Animal animal : animals) {
            animal.voice();
        }
    }

    private static void printVoices(Object... animals) {
        for (Object animal : animals) {
            System.out.println(animal.toString());
        }
    }

}
