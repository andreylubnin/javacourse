package lesson7_oop;

public class Plate {

    private int food;
    private Cat[] cats;

    public Plate (int food) {
        this.food = food;
    }

    public Plate () {
        this(0);
    }

    public int getFood () {
        return food;
    }

    public void addFood (int food) {
        this.food += food;
    }

    public void info() {
        System.out.println(this);
    }

    public void decreaseFood(int foodCount){
        if(foodCount < this.food) {
            this.food -= foodCount;
        } else {
            System.out.println("В миске кончилась еда. Нужно добавить корма!!");
        }
    }


    @Override
    public String toString () {
        return "Plate{" +
                "food=" + food +
                '}';
    }



}

