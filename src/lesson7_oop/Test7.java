package lesson7_oop;

import java.util.Random;

public class Test7 {
    private static final int PLATE_SIZE = 100;
    private static final int CATS_TOTAL_MAX = 20;
    private static final int APPETITE_MAX = 20;
    private static Plate plate;
    private static Cat[] cats;

    public static void main (String[] args) {

        getPlate();
        drawCats();
        // ---------
        plate.info();
        feedCats();
        plate.info();
        plate.addFood(100); // Observer
        plate.info();
        showCatsInfo();
    }

    public static void drawCats () {
        Random rand = new Random();
        int cat_qty = rand.nextInt(CATS_TOTAL_MAX)+1;
        cats = new Cat[cat_qty];
        for(int i = 0; i< cat_qty; i++) {
            cats[i] = new Cat("Cat"+i, rand.nextInt(APPETITE_MAX)+1);
        }
        for (Cat cat : cats) {
            cat.info();
        }
    }

    private static void showCatsInfo() {
        System.out.println("Текущий статус сытости котов: ");
        for (Cat cat : cats) {
            cat.info();
        }
    }

    private static void feedCats () {
        System.out.println("У котов наступает время обеда. Коты - все сюда!");
        for (Cat cat : cats) {
            System.out.print("Кот "+ cat.getName());
            cat.eat(plate);
        }
    }

    private static void getPlate () {
        plate = new Plate(PLATE_SIZE);
    }

    private static void getCatSatiety(Cat... cats){
        for (Cat cat : cats) {
            cat.info();
        }
    }
}
