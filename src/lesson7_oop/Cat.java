package lesson7_oop;

public class Cat {

    private final String name;
    private int appetite;
    private boolean satiety = false;

    public Cat (String name, int appetite) {
        this.name = name;
        this.appetite = appetite;
    }

    public String getName () {
        return name;
    }

    public int getAppetite () {
        return appetite;
    }

    public void eat(Plate plate) {
        if(plate.getFood()>= appetite) {
            satiety = true;
            doEat(plate);
            System.out.println(" наелся!");
        }
        else
        {
            // Thread.sleep(1000);
            int foodEat = appetite - plate.getFood();
            appetite -= foodEat;
            plate.decreaseFood(foodEat);
            System.out.println(" остался недоволен!");
        }
    }

    private void doEat (Plate plate) {
        plate.decreaseFood(appetite);
    }

    @Override
    public String toString () {
        return "Cat{" +
                "name=" + name +
                ", satiety=" + satiety +
                '}';
    }

    public void info() {
        System.out.println(this);
    }

}
