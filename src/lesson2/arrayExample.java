package lesson2;

import java.util.Arrays;

public class arrayExample {
    public static void main(String[] args) {

        final int SIZE = 5;

        int[][] data = new int[SIZE][SIZE];
//        Arrays.fill(data, );
        data[2][1] = 5;
        data[3][2] = 6;
//        data[-1] = 6;



//        System.out.println(Arrays.toString(data));

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++)
                data[i][j] = i + j;
        };

        for (int[] datum : data) {
            for (int v : datum) {
                System.out.print( v + "\t");
            }
            System.out.println();
        }

        String str = "Hello, world!";
        for (char c : str.toCharArray()) {
            System.out.println(c);
        }

        char[] charArr = {'H', 'e', 'l', 'l', 'o'};
        String str2 = new String(charArr);
        System.out.println(str2);
    }
}
