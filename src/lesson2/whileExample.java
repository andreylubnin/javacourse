package lesson2;

public class whileExample {
    public static void main(String[] args) {
        int i = 0;
        while(i<10) {

            System.out.println("WHILE "+i);

            i++;
        }


        do {
            System.out.println("DO ... WHILE "+i);
            i--;
        } while (i>0);
    }
}
