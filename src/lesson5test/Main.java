package lesson5test;

import lesson1.Calculator;
import lesson5.Cat;

public class Main {
    public static void main(String[] args) {
        Cat cat = new Cat();

        cat.setAge(777);

        System.out.println(cat);

        cat.setAge(10);

        System.out.println(cat);

        Calculator calc = new Calculator("myCalc");

        System.out.println(calc.add(2,5));
    }
}
