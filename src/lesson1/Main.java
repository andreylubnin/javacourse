package lesson1;

import lesson1_homework.homework1;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        // тестируем типы данных
        // Р.Мартина Чистый код. Чистая архитектура. Идеальный программист.
        int a = 7;
        int b = 5;
        double c = a / b;
        char chr2 = 113;

        System.out.println("Результат: " + chr2);
        System.out.println(c);

/*        char chr;
        for(int i=0; i<3000; i++) {
            System.out.printf("%4d: %2c ", i, i);
            if(i % 7 == 0) {
                System.out.println("");
            }
        };*/

        boolean iFlag = false;
        System.out.println(iFlag);

        String str2 = new String("Hello, world!");
        String str = "Hello, world!";

        System.out.println(str.toUpperCase());


//        System.out.println(add(2, 4));

       Calculator calc = new Calculator("Тестовый");

//        System.out.println(calc.mul(3,4));

        int valA = 7;
        int valB = 5;

//        System.out.println(valA % valB);
//        System.out.println(valA); // 7
//        System.out.println(valA++); // 7
//        System.out.println(++valA);  // 9
//        System.out.println(--valA); // 8
//        System.out.println(valA--); // 8
//        System.out.println(valA); // 7

        valA += valB;
        System.out.println(valA++);
        valA *= 2;
        System.out.println(++valA);

        String hw = new String("hw!");

//        if(calc.mul(3,4) == 14) {

        if(hw.equals("hw!")) {
            System.out.println("Правда!");
        } else {
            System.out.println("Ложь!");
        }
        System.out.println("Домашние задания урока 1");

        System.out.println("HW1.3 result = " + homework1.getResult(1,2,3,4));

        System.out.println("HW1.4 result = " + homework1.checkNumbers(4, 6));

        System.out.println("HW1.5 result = " + homework1.isPositiveOrNegative(-12));

        System.out.println("HW1.6 result = " + homework1.isNegative(-12));

        System.out.print("HW1.7 result: ");
        homework1.printMyName("Вася");

        System.out.print("HW1.8 result: ");
        homework1.checkIsYearLeap(2020);

    }

    public static int add(int a, int b) {

        return (a + b)*2 - 1;
    }

}
