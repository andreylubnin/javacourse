package lesson8.examples;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ex2 {
    static class MyWindow extends JFrame {
        public MyWindow(){
            setTitle("Test Window");
            setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            setBounds(400,300,400,400);

            JButton[] jbs = new JButton[5];
            for (int i = 0; i < 5; i++) {
                jbs[i] = new JButton("#"+i); // Создание кнопок
                jbs[i].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed (ActionEvent e) {
                        System.out.println(((JButton)e.getSource()).getText() + " была нажата.");
                    }
                });
            }
            setLayout(new BorderLayout()); // выбор компоновщика элементов
            add(jbs[0], BorderLayout.EAST); // Добавление кнопки на форму
            add(jbs[1], BorderLayout.WEST);
            add(jbs[2], BorderLayout.SOUTH);
            add(jbs[3], BorderLayout.NORTH);
            add(jbs[4], BorderLayout.CENTER);

            setVisible(true);
        }
    }

    public static void main (String[] args) {
        new MyWindow();
    }
}
