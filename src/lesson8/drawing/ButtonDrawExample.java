package lesson8.drawing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonDrawExample {
    static class MyWindow extends JFrame {

        public MyWindow() {
            setSize(800,800);
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setLocationRelativeTo(null);

            setLayout(new GridLayout(7,7));
            for (int i = 0; i < 49; i++) {
                add(createButton());
            }

            setVisible(true);
        }

        private JButton createButton() {
            return new JButton() {
                private boolean flag;

                // constructor for anonymous class
                {
                    addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed (ActionEvent e) {
                            flag = !flag;
                        }
                    });
                }

                @Override
                public void paint(Graphics graphics) {
                    super.paint(graphics);

                    if(flag) {
                        graphics.drawOval(this.getWidth()/4, this.getHeight()/4, getWidth()/2, getWidth()/2);
                        graphics.setColor(Color.BLUE);
                        graphics.fillOval(this.getWidth()/4,this.getHeight()/4, getWidth()/2, getWidth()/2);
                    } else {
                        Graphics2D g2d = (Graphics2D) graphics;
                        g2d.setStroke(new BasicStroke(8));
                        g2d.setColor(Color.BLUE);
                        g2d.drawLine(0,0, this.getWidth(), this.getHeight());
                        g2d.drawLine(this.getWidth(), 0, 0, this.getHeight());
                    }
                }

            };
        }
    }

    public static void main (String[] args) {
        new MyWindow();
    }
}
