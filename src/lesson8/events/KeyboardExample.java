package lesson8.events;

import lesson8.examples.Ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyboardExample {
    public static class MyWindow extends JFrame {
        public MyWindow() {
            setBounds(500,500,400,300);
            setTitle("Keyboard Event Demo");
            setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            JTextField field = new JTextField();
            add(field);
            field.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed (ActionEvent e) {
                    System.out.println("Your message: "+field.getText());
                    field.setText(null);
                }
            });

            field.addKeyListener(new KeyAdapter() {
                @Override
                public void keyTyped (KeyEvent e) {
                    if(e.isShiftDown())
                        System.out.println("Shift pressed!! ");
                    System.out.println(e.getKeyChar() + " pressed");
                }
            });

            setVisible(true);
        }
    }
    public static void main (String[] args) {
        new MyWindow();
    }
}
