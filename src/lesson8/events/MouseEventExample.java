package lesson8.events;

import javax.swing.*;
import java.awt.event.*;

public class MouseEventExample {
    public static class MyWindow extends JFrame {
        public MyWindow() {
            setBounds(500,500,400,300);
            setTitle("Keyboard Event Demo");
            setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            JPanel pan = new JPanel();
            add(pan);
            pan.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased (MouseEvent e) {
                    System.out.println("MousePos: "+e.getX()+ " " +
                            e.getY());
                }
            });

            setVisible(true);
        }
    }
    public static void main (String[] args) {
        new MyWindow();
    }
}
