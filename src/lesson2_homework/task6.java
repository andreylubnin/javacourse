package lesson2_homework;

import java.util.Arrays;

public class task6 {

    /*

7.	**** Написать метод, которому на вход подается одномерный массив и число n (может быть положительным, или отрицательным), при этом метод должен сместить все элементы массива на n позиций. Для усложнения задачи нельзя пользоваться вспомогательными массивами.


    */
    public static void main(String[] args) {
        // 6.	** Написать метод, в который передается не пустой одномерный целочисленный массив, метод должен вернуть true, если в массиве есть место, в котором сумма левой и правой части массива равны.
        // Примеры: checkBalance([2, 2, 2, 1, 2, 2, || 10, 1]) → true, checkBalance([1, 1, 1, || 2, 1]) → true, граница показана символами ||, эти символы в массив не входят.

        int[] arr = new int[]{2, 2, 2, 1, 2, 2, 10, 1};
        System.out.println("Method checkBalance for array " + Arrays.toString(arr) +  " returns " + checkBalance(arr));

//        int res = -25 % 8;
//        System.out.println("-25 % 8 = " + res);
    }


     static boolean checkBalance(int[] arr) {
        int leftSum , rightSum;
        for(int i = 0; i < arr.length; i++) {
            leftSum = 0; rightSum = 0;
            for(int j = 0; j < (i+1); j++) {
                leftSum += arr[j];
            }
            for(int k = i+1; k < (arr.length); k++) {
                rightSum += arr[k];
            }
            if(leftSum==rightSum) return true;

        }
        return false;
    }
}
