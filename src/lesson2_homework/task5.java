package lesson2_homework;

import java.util.Arrays;

public class task5 {

    /*






6.	** Написать метод, в который передается не пустой одномерный целочисленный массив, метод должен вернуть true, если в массиве есть место, в котором сумма левой и правой части массива равны. Примеры: checkBalance([2, 2, 2, 1, 2, 2, || 10, 1]) → true, checkBalance([1, 1, 1, || 2, 1]) → true, граница показана символами ||, эти символы в массив не входят.
7.	**** Написать метод, которому на вход подается одномерный массив и число n (может быть положительным, или отрицательным), при этом метод должен сместить все элементы массива на n позиций. Для усложнения задачи нельзя пользоваться вспомогательными массивами.


    */
    public static void main(String[] args) {
        l2_task5();
    }

    private static void l2_task5() {
        // 5.	** Задать одномерный массив и найти в нем минимальный и максимальный элементы (без помощи интернета);
        final int LIMIT = 15;
        int[] arr = new int[LIMIT];
        for (int i = 0; i < LIMIT; i++) {
            arr[i] = (int) (Math.random()*256);
        }
        int vMin = arr[0], vMax=arr[0];

        for (int i = 0; i< arr.length; i++) {
            if (arr[i]< vMin) vMin = arr[i];
            if (arr[i]> vMax) vMax = arr[i];
        };

        System.out.println("Source array is : " + Arrays.toString(arr));
        System.out.println("Min is : " + vMin);
        System.out.println("Max is : " + vMax);

    }
}
