package lesson2_homework;

import java.util.Arrays;

public class task7 {


    public static void main(String[] args) {

        String[] arr = new String[]{"a","b","c","d","e","f","g","h","i","j","k","l"};
        int stepsToMove = 3;
        System.out.println("Source array is : " + Arrays.toString(arr));
        System.out.println("Moving to " + stepsToMove + " steps...");
        rotateArray(arr, stepsToMove);
        System.out.println("Target array is : " + Arrays.toString(arr));
    }

    static void rotateArray(String[] arr, int cursor) {
        // 7.	**** Написать метод, которому на вход подается одномерный массив и число n (может быть положительным, или отрицательным),
        // при этом метод должен сместить все элементы массива на n позиций. Для усложнения задачи нельзя пользоваться вспомогательными массивами.

        String bufferVar;
        int cursorIndex = cursor % arr.length;
        if (cursorIndex==0) return;
        if (cursorIndex<0) cursorIndex += arr.length;

        for (int k = 1; k <= cursorIndex; k++) {
            bufferVar = arr[arr.length - 1];
            for (int i = arr.length - 1; i > 0; i--) {
                arr[i] = arr[i - 1];
            }
            arr[0] = bufferVar;
        }
    }
}
