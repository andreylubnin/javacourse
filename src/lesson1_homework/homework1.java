package lesson1_homework;

public class homework1 {

    public static void main(String[] args) {
        int intA = 1;
        String str1 = "Hello, world!";
        boolean iFlag = false;
        double doubleB = 1.0;
        float floatC = 2.0f;
        long lngD = 123_412_342_341_234L;
    };

    public static double getResult(int a, int b, int c, int d) {
        if (d==0) {
            return 0;
        }
        return a * (b + (1.0*c / d));

    }

    public static boolean checkNumbers(int a, int b) {
        return a + b >= 10 && a + b <= 20;
    };

    /*
    5.	Написать метод, которому в качестве параметра передается целое число, метод должен напечатать в консоль, положительное ли число передали или отрицательное. Замечание: ноль считаем положительным числом.
    */
    public static String isPositiveOrNegative(int a) {
        if(a>=0) {
            return "Число " + a + " положительное.";
        }
        else
        {
            return "Число " + a + " отрицательное.";
        }
    }

    //     6.	Написать метод, которому в качестве параметра передается целое число. Метод должен вернуть true, если число отрицательное.
    public static boolean isNegative(int a) {
        return a < 0;
    }

    // 7.	Написать метод, которому в качестве параметра передается строка, обозначающая имя. Метод должен вывести в консоль сообщение «Привет, указанное_имя!».
    public static void printMyName(String nameToPrint) {
        System.out.println("Привет, "+ nameToPrint + "!");
    }

     // 8.	*Написать метод, который определяет, является ли год високосным, и выводит сообщение в консоль. Каждый 4-й год является високосным, кроме каждого 100-го, при этом каждый 400-й – високосный.

    public static void checkIsYearLeap(int year) {
        String isLeap = " не високосный";
        if (year % 4 == 0 || (year / 100) % 4 == 0) isLeap = " високосный";
        System.out.println("Год " + year + isLeap);
    };
}
