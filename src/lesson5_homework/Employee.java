package lesson5_homework;

public class Employee {
    private String fullName;
    private String position;
    private String email;
    private String phone;
    private int age;
    private double salary;

    public Employee(String fullName, String position, String email, String phone, int age, double salary) {
        this.fullName = fullName != null ? fullName.toUpperCase(): null;
        this.position = position;
        this.email = email;
        if(age >= 14 && age <= 99) this.age = age;
        this.phone = phone;
        if(salary > 0.0) this.salary = salary;
    }

    public Employee(String fullName, String position, String email, String phone, int age){ this(fullName, position, email, phone, age, 0.0); }
    public Employee(String fullName, String position, String email, String phone){ this(fullName, position, email, phone, 0); }
    public Employee(String fullName, String position, String email){ this(fullName, position, email, null); }
    public Employee(String fullName, String position){ this(fullName, position, null); }
    public Employee(String fullName){ this(fullName, null); }
    public Employee(){ this(null); }

    @Override
    public String toString(){
        return "Employee {" +
                "fullName = '" + fullName + '\'' +
                ", position = '"  + position + '\'' +
                ", email = '"  + email + '\'' +
                ", phone = '"  + phone + '\'' +
                ", age = " + age +
                ", salary = " + salary +
                '}';
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }



}
