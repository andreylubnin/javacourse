package lesson5_homework;

public class Main {

    private static final int EMPLOYEE_COUNT = 5;
    private static final int MIN_AGE = 40;

    public static void main(String[] args) {

        Employee[] employeeArray = new Employee[EMPLOYEE_COUNT];
        employeeArray[0] = new Employee("Гречкин Игорь Николаевич", "Генеральный директор", "grechkin@rusnavgeo.ru", "001", 50);
        employeeArray[1] = new Employee("Равер Алексей Львович", "Коммерческий директор", "raver@rusnavgeo.ru", "200", 44);
        employeeArray[2] = new Employee("Майба Василий", "Главный бухгалтер", "maiba@rusnavgeo.ru", "100", 42);
        employeeArray[3] = new Employee("Ильин Игорь Олегович", "Технический директор", "ilin@rusnavgeo.ru", "400", 38);
        employeeArray[4] = new Employee("Сохранов Александр Сергеевич", "Директор департамента по работе с ключевыми заказчиками", "sohranov@rusnavgeo.ru", "201", 34);

        for (Employee employee : employeeArray) {
            if(employee.getAge()>=MIN_AGE) System.out.println(employee);
        }

    }


}
