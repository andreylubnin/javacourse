package lesson3_homework;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Игра Угадайка");

        do {
            int secretNumber = (int) (Math.random() * 10);
            int enteredNumber = 0;
            System.out.println("Попробуйте угадать число от 0 до 9. У вас есть три попытки.\nВведите ваше число:");

            for (int tries = 1; tries <= 3; tries++) {
                enteredNumber = scanner.nextInt();
                if (enteredNumber != secretNumber) {
                    System.out.println("Вы не угадали. Загаданное число "+ (enteredNumber < secretNumber ? "больше" : "меньше") +". У вас осталось попыток: " + (3 - tries));
                } else {
                    System.out.println("Вы угадали!");
                    break;
                }
            }
            System.out.println("Игра закончена. Было загадано число " + secretNumber + ".");
            System.out.println("Повторить игру еще раз? 1 – да / 0 – нет");
        } while(scanner.nextInt()==1);
    }
}

