package lesson3_homework;

import java.util.Arrays;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String[] words = new String[] {"apple", "orange", "lemon", "banana", "apricot", "avocado", "broccoli","carrot", "cherry", "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom","nut", "olive", "pea", "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato"};
        int secretWordIndex = (int)(Math.random()*words.length);

        System.out.println("\u001B[31m");
        System.out.println("Игра в слова. Тема: Овощи-Фрукты");
        System.out.println("\u001B[30m");

        System.out.print("Загадываем слово.... ");
        System.out.print("Слово загадано! Попробуйте его угадать. ");
        String enteredWord = "";
        char[] hintString = new char[15];
        Arrays.fill(hintString, '#');

        do {
            System.out.println("Введите ваш вариант :");
            enteredWord = scanner.next().toLowerCase();
            if(enteredWord.equals(words[secretWordIndex])) {
                System.out.println("Вы угадали! Это слово - "+words[secretWordIndex]);
            } else {
                for(int i=0; i<Math.min(enteredWord.length(),words[secretWordIndex].length()); i++){
                    if(enteredWord.charAt(i)==words[secretWordIndex].charAt(i)) hintString[i] = enteredWord.charAt(i);
                }
                System.out.print("Вы не угадали. Подсказка, это слово похоже на: ");
                for (char v : hintString) {
                    System.out.print(v);
                }
                System.out.print(". Попробуйте ещё раз. ");
            }
        } while (!enteredWord.equals(words[secretWordIndex]));
        System.out.println("Поздравляем, вы угадали слово!");

    }
}
